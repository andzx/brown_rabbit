// Select slider
const slides = document.querySelectorAll(".carousel-item");

// Activate slider with a random slide start
slides[Math.floor(Math.random() * slides.length)].classList.add("active");

// Save page html for later
const menuHTML = $(".nav-main").html();
const introHTML = $(".intro").html();
const footerHTML = $("footer").html();
const asideHTML = $("aside").html();

// Save page number
let currentPage = 1;

// Save the text current being highlighted
let currentHighlightedText = "";

function search() {
    // Get search input data
    const searchInput = $(".search").val();
    
    // Make a new regexp replacer
    const replacer = new RegExp(searchInput, 'g');
    
    // Add highlighting to any matching text on the page
    const newMenuHTML = menuHTML.replace(replacer, "<span class='highlight'>" + searchInput + "</span>");
    const newIntroHTML = introHTML.replace(replacer, "<span class='highlight'>" + searchInput + "</span>");
    const newFooterHTML = footerHTML.replace(replacer, "<span class='highlight'>" + searchInput + "</span>");
    const newAsideHTML = asideHTML.replace(replacer, "<span class='highlight'>" + searchInput + "</span>");
    
    // Re-Render the page
    $(".nav-main").html(newMenuHTML);
    $(".intro").html(newIntroHTML);
    $("footer").html(newFooterHTML);
    $("aside").html(newAsideHTML);
    
    // Re-input the search input
    $(".search").val(searchInput);
    
    // Remember the currently highlighted text
    currentHighlightedText = searchInput;
    
    // Re-initialize the page functionality
    init();
}

// Posts
const posts = [
    {
        "headline": "Wonderful Copenhagen 2011",
        "imageSrc": "post1_pic.png",
        "imageAlt": "post 1 pic",
        "date": "Posted: 23/1-2011",
        "post": "The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory. ...",
        "readMore": "Some extra text here. Some more extra text here. And even more extra text here."
    },
    {
        "headline": "Nordic Barista Cup 2011 in Copenhagen",
        "imageSrc": "post2_pic.png",
        "imageAlt": "post 2 pic",
        "date": "Posted: 22/1-2011",
        "post": "Nordic Barista Cup 2011 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page. ...",
        "readMore": "A different extra text here. Some more different extra text here. And even more different extra text here."
    },
    {
        "headline": "2010 Winners: Sweden",
        "imageSrc": "post3_pic.png",
        "imageAlt": "post 3 pic",
        "date": "Posted: 11/1-2011",
        "post": "Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. ...",
        "readMore": "Yet more extra text here. And yet more extra text here. And yet more."
    },
    
    // Extra test posts
    {
        "headline": "Wonderful Copenhagen 2020",
        "imageSrc": "post4_pic.png",
        "imageAlt": "post 4 pic",
        "date": "Posted: 19/1-2020",
        "post": "The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory. ...",
        "readMore": "Some extra text here. Some more extra text here. And even more extra text here."
    },
    {
        "headline": "Nordic Barista Cup 2020 in Copenhagen",
        "imageSrc": "post5_pic.png",
        "imageAlt": "post 5 pic",
        "date": "Posted: 17/1-2020",
        "post": "Nordic Barista Cup 2020 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2020. The theme for the 2020 seminar is: SENSORY. More information will follow on this page. ...",
        "readMore": "A different extra text here. Some more different extra text here. And even more different extra text here."
    },
    {
        "headline": "2020 Winners: Sweden",
        "imageSrc": "post6_pic.png",
        "imageAlt": "post 6 pic",
        "date": "Posted: 15/1-2020",
        "post": "Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. ...",
        "readMore": "Yet more extra text here. And yet more extra text here. And yet more."
    },
    {
        "headline": "Wonderful Copenhagen 2020",
        "imageSrc": "post7_pic.png",
        "imageAlt": "post 7 pic",
        "date": "Posted: 23/1-2020",
        "post": "The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory. ...",
        "readMore": "Some extra text here. Some more extra text here. And even more extra text here."
    },
    {
        "headline": "Nordic Barista Cup 2020 in Copenhagen",
        "imageSrc": "post8_pic.png",
        "imageAlt": "post 8 pic",
        "date": "Posted: 22/1-2020",
        "post": "Nordic Barista Cup 2020 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2020. The theme for the 2020 seminar is: SENSORY. More information will follow on this page. ...",
        "readMore": "A different extra text here. Some more different extra text here. And even more different extra text here."
    },
    {
        "headline": "2010 Winners: Sweden",
        "imageSrc": "post9_pic.png",
        "imageAlt": "post 9 pic",
        "date": "Posted: 11/1-2020",
        "post": "Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. ...",
        "readMore": "Yet more extra text here. And yet more extra text here. And yet more."
    },
    {
        "headline": "Wonderful Copenhagen 2020",
        "imageSrc": "post10_pic.png",
        "imageAlt": "post 10 pic",
        "date": "Posted: 23/1-2020",
        "post": "The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory. ...",
        "readMore": "Some extra text here. Some more extra text here. And even more extra text here."
    },
    {
        "headline": "Nordic Barista Cup 2020 in Copenhagen",
        "imageSrc": "post11_pic.png",
        "imageAlt": "post 11 pic",
        "date": "Posted: 22/1-2020",
        "post": "Nordic Barista Cup 2020 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2020. The theme for the 2020 seminar is: SENSORY. More information will follow on this page. ...",
        "readMore": "A different extra text here. Some more different extra text here. And even more different extra text here."
    },
    {
        "headline": "2020 Winners: Sweden",
        "imageSrc": "post12_pic.png",
        "imageAlt": "post 12 pic",
        "date": "Posted: 11/1-2020",
        "post": "Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. ...",
        "readMore": "Yet more extra text here. And yet more extra text here. And yet more."
    },
    {
        "headline": "Wonderful Copenhagen 2020",
        "imageSrc": "post13_pic.png",
        "imageAlt": "post 13 pic",
        "date": "Posted: 23/1-2020",
        "post": "The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory. ...",
        "readMore": "Some extra text here. Some more extra text here. And even more extra text here."
    },
    {
        "headline": "Nordic Barista Cup 2020 in Copenhagen",
        "imageSrc": "post14_pic.png",
        "imageAlt": "post 14 pic",
        "date": "Posted: 22/1-2020",
        "post": "Nordic Barista Cup 2020 will be held in Copenhagen, Denmark. Dates: 25th - 27th August 2020. The theme for the 2020 seminar is: SENSORY. More information will follow on this page. ...",
        "readMore": "A different extra text here. Some more different extra text here. And even more different extra text here."
    },
    {
        "headline": "2020 Winners: Sweden",
        "imageSrc": "post15_pic.png",
        "imageAlt": "post 15 pic",
        "date": "Posted: 11/1-2020",
        "post": "Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here. ...",
        "readMore": "Yet more extra text here. And yet more extra text here. And yet more."
    }
];

function renderPosts() {
    const postTemplate = "\
    \
    <div class=\"post\">\
        <div class=\"row\">\
            <div class=\"col-lg-3\">\
                <div class=\"post-pic\">\
                    <div class=\"post-pic-frame\">\
                        <img src=\"img\/{{imageSrc}}\" alt=\"{{imageAlt}}\">\
                    </div>\
                </div>\
            </div>\
            \
            <div class=\"col-lg-9\">\
                <div class=\"post-content\">\
                    <h2>{{headline}}</h2>\
                    <div class=\"post-date\">{{date}}</div>\
                    <p class=\"post{{postId}}\" data-read-more=\"{{readMore}}\">{{post}}</p>\
                    <button type=\"button\" class=\"btn-read-more\" data-id=\"{{postId}}\">Read more</button>\
                </div>\
            </div>\
        </div>\
    </div>";
    
    let mainContent = "";
    
    // Compile the visible posts html
    for (let i = (currentPage * 3) - 3; i < (currentPage * 3); i++) {
        let tempPostTemplate = postTemplate;
        
        // Add post data to the temporary post template
        tempPostTemplate = tempPostTemplate.replace(new RegExp("{{postId}}", 'g'), i + 1);
        tempPostTemplate = tempPostTemplate.replace("{{headline}}", posts[i].headline);
        tempPostTemplate = tempPostTemplate.replace("{{post}}", posts[i].post);
        tempPostTemplate = tempPostTemplate.replace("{{date}}", posts[i].date);
        tempPostTemplate = tempPostTemplate.replace("{{imageSrc}}", posts[i].imageSrc);
        tempPostTemplate = tempPostTemplate.replace("{{imageAlt}}", posts[i].imageAlt);
        tempPostTemplate = tempPostTemplate.replace("{{readMore}}", posts[i].readMore);
        
        // Add divider after the post
        tempPostTemplate += "<div class=\"divider\"></div>";

        // Add post html to main content html
        mainContent += tempPostTemplate;
    }
    
    // Generate pagination html
    let pagination = "<nav class=\"nav-pg\">";
    pagination += "<div class=\"pg-start\">Page " + currentPage + " of " + Math.ceil(posts.length / 3) + "</div>";

    for (let i = 0; i < (posts.length / 3); i++) {
        if (currentPage == (i + 1)) {
            pagination += "<div class=\"pg-page pg-active\">" + (i + 1) + "</div>";
            continue;
        }

        pagination += "<div class=\"pg-page\">" + (i + 1) + "</div>";
    }

    pagination += "<div class=\"pg-end\">&raquo;</div>";
    pagination += "</nav>";
    
    // Add pagination to main content
    mainContent += pagination;
    
    // Highlight text if there is anything being highlighted right now
    if (currentHighlightedText !== "") {
        mainContent = mainContent.replaceAll(currentHighlightedText, "<span class='highlight'>" + currentHighlightedText + "</span>");
    }

    // Populate the main content
    $(".main-content > .row > .col-lg-9").html(mainContent);

    // Add read more functionality
    $(".btn-read-more").on("click", function() {
        // Make a postid for easier code reading
        const postTextId = ".post" + $(this).data("id");
        
        // Get new post text
        const newPostText = $(postTextId).html() + $(postTextId).data("readMore");
        
        // Insert text
        $(postTextId).html(newPostText.replace("...", ""));
        
        // Remove the read-more button
        $(this).remove();
    });
    
    // Add pagination functionality
    $(".pg-page").on("click", function() {
        // Change the page
        currentPage = $(this).text();

        // Re-initialize the page
        init();
    });

}

// Initialize the page functionality
function init() {
    // Search functionality, on enter key
    $(".search").on("keyup", (e) => {
        if (e.key === 'Enter' || e.keyCode === 13) {
            search();
        }
    });

    // Search functionality when clicking the search button
    $(".btn-search").on("click", () => {
        search();
    });
    
    // Render the posts and pagination
    renderPosts();
}

init();